from selenium import webdriver
from selenium.webdriver.common.keys import Keys

d = webdriver.Firefox(executable_path = 'C://geckodriver.exe')
d.get('https://youtube.com/')

assert "YouTube" in d.title

elem = d.find_element_by_name("q")
elem.clear()
elem.send_keys("youtube")
elem.send_keys(Keys.RETURN)
d.close()
