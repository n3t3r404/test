import logging
import PySimpleGUI as sg

logging.basicConfig(filename="C:/Repos/log/icon_test/log.log",
                    format='[ %(asctime)s | %(levelname)s ] - %(message)s',
                    level=logging.DEBUG,
                    filemode='w')
log = logging.getLogger()
log.info("logging started.")

sg.theme('black')

layout = [
    [sg.Button('OK')]
]

window = sg.window('test', layout)
