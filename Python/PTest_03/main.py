import sys
from time import sleep
import logging

f = '%(asctime)-s || %(levelname)-s || %(message)-s '
df = ' %H : %M :%S || %d-%m-%Y '

logging.basicConfig(filename="C:\\log\\PTest_03.log", level=logging.DEBUG, filemode='w', format=f, datefmt=df)

log = logging.getLogger()

log.debug('logger started')


class Human:
    """ Initialise human being"""
    def __init__(self, name, age):
        """ Get the user name """
        self.name = name
        self.age = age
        log.debug('class def passed')
        pass


n = input('Please enter your correct name .. \n')
a = input('Please enter your correct age .. \n')

person = Human(n, a)

print('So you are', person.name, 'and you are', person.age, 'year old')
log.debug('app ended')

sleep(2)
log.debug('app exit')
sys.exit('bye !')
