#Test 01
#f = open("C:/Test/Python/Text/Test.txt")
#text = f.read()
#f.close()
#print(text)

#Test 02
#with open("C:/Test/Python/Text/Test.txt") as fobj:
#    text = fobj.read()
#print(text)

#Test 03
#try:
#    with open("C:/Test/Python/Text/Test.txt") as f:
#        text = f.read()
#except FileNotFoundError:
#    text = None
#print(text)

#Test 04
#names = ["May", "George", "Rayan", "Lara"]
#with open("C:/Test/Python/Text/Names.txt", "w") as f:
#    for name in names:
#        f.write(name)
#        f.write("\n")
