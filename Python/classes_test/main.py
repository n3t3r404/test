class Person:
    def __init__(myobject, name, age):
        myobject.name = name
        myobject.age = age

    def myfunc(abc):
        print('Hello my name is ', abc.name)


p1 = Person('John', 36)
p1.myfunc()
