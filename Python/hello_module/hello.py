# define hello.world()
def world():
    print('Hello World \n')

# define hello.name()


def name():
    n = input('print the name ... \n')
    print('Hello ', n, '\n')


# define hello.me()


def me():
    m = input('print your name ... \n')
    print('Hello ', m, '\n')


# define hello.everyone()


def everyone(names):
    print('Hello ', names, '\n')


# make hello.root
root = 'root'

# ----- END -----
