import logging

# logger setting
fmt = "%(LevelName)s %(AscTime)s - %(Message)s"
logging.basicConfig(filename='C:\\log\\PTest_02.log',
                    level=logging.DEBUG,
                    filemode='w',
                    format=fmt)
log = logging.getLogger()
log.info('The logger is set.')
