import logging
import math

FORMAT = "%(levelname)s %(asctime)s - %(message)s "
logging.basicConfig(filename = "C:\\log\\log.log",
                    level = logging.DEBUG,
                    format = FORMAT,
                    filemode = 'w')
logger = logging.getLogger()

def volume(r):
    """ Returns the volume of a sphere with radius r """
    v = (4.0/3.0) * math.pi *r**3
    return v
print(volume(2))

#End
logger.debug("End1 !")