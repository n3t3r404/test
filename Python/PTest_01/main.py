import logging

# blank integers
usr = None

# set logger
f = "%(LevelName)s %(AscTime)s - %(Message)s "
logging.basicConfig(filename='C:\\log\\PTest1.log',
                    level=logging.DEBUG,
                    format=f,
                    filemode='w')
log = logging.getLogger(__name__)


def name_log(user):
    """ log the user name """
    return log.debug('User name is :', user)


usr = input('User enter your name please :\n')
name_log(usr)
