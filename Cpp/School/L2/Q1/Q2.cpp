#include <stdio.h>
#include "Q2.h"

int foo(int n)
{
	int count = 0;
	while (n != 0)
	{
		count++;
		n &= n - 1;
	}
	return count;
}
