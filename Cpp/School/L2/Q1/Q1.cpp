#include <stdio.h>

// A1a
/*
void main()
{
    int a, b;
    a = 2;
    b = (a & 1) * a++;
    printf("%d \n",b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 0

// A1b
/*
void main()
{
    int a, b;
    a = 2;
    b = a & 1 * a++;
    printf("%d\n", b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 2

//A2a
/*
void main()
{
    int a, b;
    a = 2;
    b = a | (a | a) & (a ^ a);
    printf("%d \n", b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 2

//A2b
/*
void main()
{
    int a, b;
    a = 2;
    b = a | a | a & a ^ a;
    printf("%d \n", b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 2

//A3a
/*
void main()
{
    int a, b;
    a = 2;
    b = a | (a | a) | a;
    printf("%d\n", b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 2

//A3b
/*
void main()
{
    int a, b;
    a = 2;
    b = a | a | a | a;
    printf("%d\n", b);
}
*/
// a = 1 | a = 2
// b = 1 | b = 2
