package com.root;

public class Student
{
    private String name;

    public String getName()
    {

        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public static void main(String[] args)
    {
        Student s = new Student();
        s.setName("root");
        String name = s.getName();
    }
}
