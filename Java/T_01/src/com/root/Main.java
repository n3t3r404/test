package com.root;

public class Main
{
    private String name;

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public static void main(String[] args)
    {
        Main s = new Main();
        s.setName("root");
        String name = s.getName();
    }
}
