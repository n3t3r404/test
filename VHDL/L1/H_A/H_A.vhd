LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY H_A IS
PORT
(
	a : IN STD_LOGIC;
	b : IN STD_LOGIC;
	s : OUT STD_LOGIC;
	c : OUT STD_LOGIC);

END H_A;

ARCHITECTURE H_A_arc OF H_A IS
BEGIN
	s <= a xor b;
	c <= a and b;
END H_A_arc;
