LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY fba IS
PORT(
	A,B: IN STD_LOGIC_vector(3 downto 0);
	Cin: in std_LOGIC;
	S : OUT STD_LOGIC_vector(3 downto 0);
	Cout : OUT STD_LOGIC);
END
--  Architecture Body
ARCHITECTURE fba_arc OF fba IS
 signal sc0,sc1,sc2;std_LOGIC;
 component F_A is
	port(
		a,b,cin: in std_LOGIC;
		s,cout: out std_LOGIC);
	end component
BEGIN
u1:F_A
port map(
	a=>A(0),
	b=>B(0),
	cin=>Cin,
	s=>S(0),
cout=>sc0);

u2:F_A
port map(
	a=>A(1),
	b=>B(1),
	cin=>Cin,
	s=>S(1),
cout=>sc1);

u3:F_A
port map(
	a=>A(2),
	b=>B(2),
	cin=>Cin,
	s=>S(2),
cout=>sc2);

u4:F_A
port map(
	a=>A(3),
	b=>B(3),
	cin=>Cin,
	s=>S(3),
	cout=>cout);
END ;
