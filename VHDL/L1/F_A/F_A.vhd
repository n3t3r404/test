LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY F_A IS
-- Entity F_A
PORT
(
	a : IN STD_LOGIC;
	b : IN STD_LOGIC;
	cin : IN STD_LOGIC;
	s : OUT STD_LOGIC;
	cout : OUT STD_LOGIC	
);
END F_A;

ARCHITECTURE F_A_arc OF F_A IS
-- Architecture F_A
	signal s1, s2, s3:std_logic;
	component H_A is
	port(
		a, b:in std_logic;
		s, c:out std_logic);
	end component;
	begin
	
	u1: H_A
	port map(
	a => a,
	b => b,
	s =>s2,
	c => s1);
	
	u2: H_A
	port map(
	a => s2,
	b => cin,
	s => s,
	c => s3);
	
	cout <= s1 or s3;
end;
