LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY H_A IS

PORT
(
a : IN STD_LOGIC;
b : IN STD_LOGIC;
s : OUT STD_LOGIC;
cout : OUT STD_LOGIC
);

END H_A;

ARCHITECTURE H_A_architecture OF H_A IS

BEGIN

s <= a xor b;
cout <= a and b;
 
END H_A_architecture;
